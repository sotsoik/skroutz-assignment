<?php
	//Check if the script is running in Command Line
	if (php_sapi_name() == "cli") {
		
		//Remove script argument
		array_shift($argv);

		//check if there are any argument
		if (count($argv)) {

			// Get First argument as username
			$user = $argv[0];

			//Get data from Github API
			$curl_handle=curl_init();
			
			curl_setopt($curl_handle, CURLOPT_URL,'https://api.github.com/users/'.$user.'/repos?type=public');
			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
			
			$query = curl_exec($curl_handle);
			
			curl_close($curl_handle);

			$response = json_decode($query);

			// Check if there is an error message in the query
			if(!is_array($response) && $response->message){
				die("No Data Found! Are you sure this is the correct username?\n");
			}else{

				//Create an array for Initial Data
				$arr = array();

				$count = 0;

				//Iterate through array to get langueages from non-forked projects.
				foreach ($response as $key => $item) {
					if($item->forks != 0){
						continue;
					}else{
						// In case the API is not returning a Language
						if($item->language == ''){
							$arr['Unknown'][$key] = 'Unknown';
							$count++;
						}else{
							$arr[$item->language][$key] = $item->language;
							$count++;
						}
					}
				}

				// Create an array to Group Languages and calculate percentage
				$final = array();

				// Sort Array
				arsort($arr);

				foreach ($arr as $key => $el){
					$percent = ((int)count($el) / (int)$count) * 100;

					$percent = round($percent, 2);
					echo($key." ==> Total: ".count($el)." | Percentage: ".$percent."%\n");
					$final[$key] = array('val' => count($el),
										'percentage' => $percent);
				}
			}
		}else{
			die("There are no arguments passed!\n");
		}
	}else{
		die("Please run the script using command line!\n");
	}

?>